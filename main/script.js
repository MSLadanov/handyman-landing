document.addEventListener('DOMContentLoaded', () => {
  function toggleHeader() {
    if (document.querySelector('.tablet-menu').className.includes('toggled')) {
      document.querySelector('.tablet-menu').style.maxHeight = null
      document.querySelector('.tablet-menu').classList.remove('toggled')
    } else {
      document.querySelector('.tablet-menu').classList.add('toggled')
      document.querySelector('.tablet-menu').style.maxHeight =
        document.querySelector('.tablet-menu').scrollHeight + 'px'
    }
  }

  document.addEventListener('click', (e) => {
    if (!e.composedPath().includes(document.querySelector('header'))) {
      document.querySelector('.tablet-menu').style.maxHeight = null
    }
  })

  document
    .querySelector('.menu-toggler')
    .addEventListener('click', toggleHeader)
  document.querySelector('.menu-toggler').addEventListener('mouseover', () => {
    document
      .querySelectorAll('.toggler-el')
      .forEach((item) => item.classList.add('menu-toggler-focused'))
  })
  document.querySelector('.menu-toggler').addEventListener('mouseout', () => {
    document
      .querySelectorAll('.toggler-el')
      .forEach((item) => item.classList.remove('menu-toggler-focused'))
  })
})
